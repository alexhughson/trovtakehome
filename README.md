# Alex Hughson - Gilded Rose exercise

## Running project

### Requirements

Requires python (python2) and django.
https://docs.djangoproject.com/en/1.10/topics/install/

### Running

Navigate to project and run

```
python manage.py migrate
python manage.py runserver
```

this will start a server at `localhost:8000` assuming Django defaults are still in play

### Running tests

Navigate to project and run `python manage.py test`

## Basic stuff

### Assumptions

- Any member of the public can see the list of inventory
- Each item in the stock is totally unique, and only one can be sent to any merchant
- A web based login cannot be required for the API.  Since POS devices will be the most likely clients
- Items sold by the Gilded Rose are in such high demand that it would be reasonable to expect 2 near-simultaneous requests for the same item
- There is some secure mechanism by which secret keys can be transmitted to merchants (out of scope, but I would probably implement a show once dashboard)

### Design Decisions

- Authentication will be done using HMAC signing of request data, with a secret key distributed to merchants.  Merchants are expected to know both their id and secret_key, though only the id will be transmitted on the wire
- Ensuring that an item can only be ordered once will be implemented using database UNIQUE constraints, to automate using a single database operation to lock products.  ItemOrder rows will be created, and when one exists an item will be considered to be ordered.
- Customers have a name, but it is not used.  Names seem nice

### Ignored faults

These faults exist and have straightforward resolutions which I did not pursue

- content_type is presumed to be application/json on the endpoints, but not validated
- Pagination is not implemented, but the results data structure will allow the extension without massive changes
- Did not use an API framework, since this is a tightly constrained project
- Item names can only be 255 characters (makes the field easier to seach/index)
- Prices will be limited to $2,147,483,647, which could be a problem depending how upmarket the Gilded Rose is
- The HTML on the test page is simply abysmal.
- Also, the Javascript no that page is horrid, I promise that I normally do better than this, but that part of the project is just hacked together for demonstrations sake

### Test site usage

#### Setting up Data

You can use the Django admin at /admin to generate Items and Customers.

First, create a superuser by running `python manage.py createsuperuser` and following the prompts.

Documentation on the admin interface can be found here https://docs.djangoproject.com/en/1.10/intro/tutorial02/#explore-the-free-admin-functionality

You can log into the admin site and create Items, and create Customers with secret keys that you input manually.  There is no good way to get the customer id, but the first one you create will have the id 1, the second one will have the id 2, etc.

#### Using the test site

The test site is really awful, but it shows a simplistic client for the endpoints.
You can see it by navigating to `localhost:8000`

Clicking on `Show Inventory` will make a `GET` to the `/items/` endpoint and show the results.

When you fill in a secret key, customer id, and item id and hit `Order Item`, it will construct an authenticated request to the `/buy/` api.  Very little data is displayed for this, but it is logged in the console if you want to go look.

### Notes

This is built using Python/Django because that is the framework I am most comfortable these days.

## Spec

### Items Endpoint

The items endpoint is availiable at `/items/`, can only be accessed through `GET` requests, and returns data in the format

```
{
    "results": [
        {
            "id": int,
            "Name": "string",
            "Description": "string",
            "Price": int
        },
        ...
    ]
}
```

#### Example request

```
GET /items/ HTTP/1.1
Host: localhost:8000
...
```

expected response

```
HTTP/1.1 200 OK
Content-Type: application/json
...

{"results": [{"id": 1, "Name": "ItemName", "Description": "ItemDescription", "Price": "20.00"}]}
```

### Buy endpoint

this endpoint is availiable at `/buy/`, and it only accepts `POST` requests

This endpoint accepts a JSON object with the keys `item_id` and `customer_id`.  The endpoint also requires an Authorization header which contains the json body content of the request HMAC-SHA256 encoded with the customer's secret key.

Returns 200 if the order is successful.

#### Example request

Assuming that the secret key for customer 1 is 'secret', the HMAC-SHA256 of the body JSON with 'secret' is a17e01eedbb4c5df38c6e8afed492103d10e760b7e8570952465d859dbc87e3b

```
POST /buy/ HTTP/1.1
Host: localhost:8000
Content-Type: application/json
Content-Length: 32
Authorization: a17e01eedbb4c5df38c6e8afed492103d10e760b7e8570952465d859dbc87e3b

{"customer_id": 1, "item_id": 1}
```

expected response

```
HTTP/1.1 200 OK
...

Order Processed
```

## Answers to your questions

We know a user is authenticated if they can HMAC sign their request data.

It is not always possible to buy an item, you can't buy an item if you aren't authorized, and you can't buy an item if it has already been sold.

3) a). I chose JSON as a data format, just as a default, it seems like the standard format for most RESTish apis these days and anything familiar is best

3) b)  I chose HMAC authentication, as it allows for a wide variety of potential clients, and keeps secret key data off of the wire.  While this format has the limitation that somebody who compromises the Gilded Rose database can access the authenticator for all users, I would probably store it in an encrypted form in a more productiony environment.  Its wide application in the world indicates that this is the sort of risk that can be accepted with reasonable security practices.