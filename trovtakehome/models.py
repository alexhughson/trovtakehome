from django.contrib import admin
from django.db import models


class Item(models.Model):
    Name = models.CharField(max_length=255)
    Description = models.TextField()
    Price = models.IntegerField()

    def __str__(self):
        return self.Name


class Customer(models.Model):
    name = models.TextField()
    secret_key = models.TextField()


class ItemOrder(models.Model):
    customer = models.ForeignKey(Customer)
    item = models.ForeignKey(Item, unique=True)


admin.site.register(Item)
admin.site.register(Customer)
admin.site.register(ItemOrder)