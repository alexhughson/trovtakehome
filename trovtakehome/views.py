import json

from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.http import (
    JsonResponse,
    HttpResponse,
    HttpResponseNotAllowed,
    HttpResponseBadRequest,
    HttpResponseForbidden,
    HttpResponseNotFound,
)

from authentication import is_authenticated
from models import Item, Customer, ItemOrder

class HttpRespondImmediatelyException(Exception):
    def __init__(self, http_response):
        self.http_response = http_response

def handles_raised_responses(fn):
    def ret_fn(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except HttpRespondImmediatelyException as HttpRaise:
            return HttpRaise.http_response
    return ret_fn

def get_items(request):
    if request.method != 'GET':
        return HttpResponseNotAllowed(['GET'])

    items = Item.objects.filter(itemorder=None)
    item_list = []
    for item in items:
        item_list.append({
            "id": item.id,
            "Name": item.Name,
            "Description": item.Description,
            "Price": item.Price,
        })

    response_struct = {
        'results': item_list,
    }
    return JsonResponse(response_struct)

def _validate_request(request):
    if request.method != 'POST':
        raise HttpRespondImmediatelyException(HttpResponseNotAllowed(['POST']))

    bodytext = request.body
    try:
        body_json = json.loads(bodytext)
    except ValueError:
        raise HttpRespondImmediatelyException(HttpResponseBadRequest('Request was not JSON'))

    try:
        customer_id = body_json['customer_id']
        item_id = body_json['item_id']
        authorization_header = request.META['HTTP_AUTHORIZATION']
    except KeyError:
        raise HttpRespondImmediatelyException(HttpResponseBadRequest('Request missing data'))



    return customer_id, item_id, authorization_header

def _authorize_request(body_text, customer_id, item_id, authorization_header):
    try:
        customer_record = Customer.objects.get(id=customer_id)
    except ObjectDoesNotExist:
        raise HttpRespondImmediatelyException(
            HttpResponse('Not Authorized', status=401)
        )

    authenticated = is_authenticated(
        body_text,
        str(customer_record.secret_key),
        authorization_header
    )
    if not authenticated:
        raise HttpRespondImmediatelyException(HttpResponse('Not Authorized', status=401))


    return True


def _attempt_to_order_item(customer_id, item_id):
    # This only applies on sqlite, which does not have key constraints
    if not Item.objects.filter(id=item_id).exists():
        raise HttpRespondImmediatelyException(HttpResponseNotFound('No such item'))
    try:
        ItemOrder.objects.create(
            customer_id=customer_id,
            item_id=item_id,
        )
    except IntegrityError:
        raise HttpRespondImmediatelyException(
            HttpResponseForbidden('That item has already been ordered')
        )

@handles_raised_responses
def buy(request):
    _validate_request(request)

    body_text = request.body
    body_json = json.loads(body_text)
    customer_id = body_json['customer_id']
    item_id = body_json['item_id']
    authorization_header = request.META['HTTP_AUTHORIZATION']

    _authorize_request(body_text, customer_id, item_id, authorization_header)

    _attempt_to_order_item(customer_id, item_id)

    return HttpResponse('Order Processed')


