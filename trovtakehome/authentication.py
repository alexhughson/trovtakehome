import hashlib
import hmac

def is_authenticated(input_string, secret_key, authentication_code):
    thehash = hash_string(input_string, secret_key)
    return authentication_code == thehash


def hash_string(input_string, secret_key):
    hasher = hmac.new(secret_key, input_string, hashlib.sha256)
    return hasher.hexdigest()
